
Object.prototype[Symbol.toPrimitive] = function (hint) {
  console.log(` Преобразование типа: ${hint}`);

  if (hint == "string") {
   return toString(this); 
  } 
  return valueOf(this);

  function isPrimitive(value) {
    return (value !== Object(value));
  };

  function toString(value) {
    if (isPrimitive(value.toString())) {
      return value.toString();
    }
    if(isPrimitive(value.valueOf())) {
      return value.valueOf();
    }
    throw new TypeError("Cannot convert object to primitive value")
  }
  function valueOf(value) {
    if (isPrimitive(value.valueOf())) {
      return value.valueOf();
    }
    if(isPrimitive(value.toString())) {
      return value.toString();
    }
    throw new TypeError("Cannot convert object to primitive value")
  }
    
}

console.log([2] + [2, 1]);
console.log(true + {});
console.log(String([2]));
console.log("2" + String({a:2}))
console.log([] - {});
console.log([] / 5)
